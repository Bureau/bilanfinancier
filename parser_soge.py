#!/usr/bin/python3

import enum
import re
import subprocess
import sys


class Mode(enum.Enum):
	NextPage = 0
	ReadOp = 1


def parse(releve, verbose=False):
	lines = releve.split(b'\n')

	# DATE, VALEUR, OPERATION, DEBIT, CREDIT
	operations = []

	mode = Mode.NextPage
	params = {}

	for i in range(len(lines)):
		line = lines[i].decode('utf-8', 'ignore')
		if not line:
			continue
		if verbose: print(line, file=sys.stderr)
		if line.__contains__('Date') and line.__contains__('Valeur') and line.__contains__("Nature de l'opération") and line.__contains__('Débit') and line.__contains__('Crédit'):
			if verbose: print('New page at line {} : {}'.format(i, line), file=sys.stderr)
			mode = Mode.ReadOp
			params = {
				'date_start': line.index('Date'),
				'valeur_start': line.index('Valeur'),
				'operation_start': line.index("Nature de l'opération"),
				'debit_start': line.index('Débit'),
				'credit_start': line.index('Crédit'),
				'max_length': len(line),
			}
		elif line.strip() == 'SOLDE PRÉCÉDENT AU':
			continue
		elif mode == Mode.ReadOp:
			if line[0] == ' ' and line[1] in map(chr, range(ord('0'), ord('9')+1)):
				m = re.fullmatch(r'^\s+(\d\d/\d\d/\d\d\d\d)\s+(\d\d/\d\d/\d\d\d\d)\s+(.*)\s+([0-9.]+,\d+)\n?$', line)
				if len(line) <= params['credit_start']:
					operations.append([m.group(1), m.group(2), m.group(3).strip()+'\n', m.group(4), None])
				else:
					operations.append([m.group(1), m.group(2), m.group(3).strip()+'\n', None, m.group(4)])
				params['max_length'] = max(params['max_length'], len(line))
			elif line.isspace():
				mode = Mode.NextPage
			elif line.index(line.strip()) > params['max_length']:
				mode = Mode.NextPage
			elif params['valeur_start'] <= line.index(line.strip()) <= params['operation_start']:
				operations[-1][2] += line.strip() + '\n'

	return operations

def post_process(operations):
	for line in operations:
		if line[3]:
			line[3] = line[3].replace('.', '').replace(',', '.')
		if line[4]:
			line[4] = line[4].replace('.', '').replace(',', '.')
		line[2] = '"'+line[2]+'"'
	return operations

if __name__ == '__main__':
	releve = subprocess.check_output(['pdftotext', '-layout', sys.argv[1], '-'])
	for operation in post_process(parse(releve)):
		print(';'.join(map(str, operation)))
